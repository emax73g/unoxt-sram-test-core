////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: memtest_translate.v
// /___/   /\     Timestamp: Mon Aug 23 17:50:34 2021
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/translate -ofmt verilog -sim memtest.ngd memtest_translate.v 
// Device	: 6slx16ftg256-2
// Input file	: memtest.ngd
// Output file	: /home/ise/share/memtest/memtest/netgen/translate/memtest_translate.v
// # of Modules	: 1
// Design Name	: memtest
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module memtest (
  nReset, clock, nRamOE, nRamWE, nRamCE, nRamLB, nRamUB, ledRed, ledYellow, ledGreen, ramAddr, ramData
);
  input nReset;
  input clock;
  output nRamOE;
  output nRamWE;
  output nRamCE;
  output nRamLB;
  output nRamUB;
  output ledRed;
  output ledYellow;
  output ledGreen;
  output [19 : 0] ramAddr;
  inout [15 : 0] ramData;
  wire nReset_IBUF_67;
  wire clock_BUFGP;
  wire _n0158;
  wire ledGreen_OBUF_86;
  wire ledRed_OBUF_87;
  wire \state[15]_GND_1_o_Select_58_o ;
  wire \state[15]_GND_1_o_Select_60_o ;
  wire \state[15]_GND_1_o_Select_62_o ;
  wire \state[15]_PWR_1_o_Select_64_o ;
  wire nextState_15_92;
  wire nextState_14_93;
  wire nextState_13_94;
  wire nextState_12_95;
  wire nextState_11_96;
  wire nextState_10_97;
  wire nextState_9_98;
  wire nextState_8_99;
  wire nextState_7_100;
  wire nextState_6_101;
  wire nextState_5_102;
  wire nextState_4_103;
  wire nextState_3_104;
  wire addrZero;
  wire addrInc_BUFG_106;
  wire nextState_2_107;
  wire nextState_1_108;
  wire nRamWE_OBUF_109;
  wire nextState_0_110;
  wire _n0233;
  wire nRamOE_OBUF_129;
  wire ledYellow_OBUF_130;
  wire nRamCE_OBUF_131;
  wire \ramData[15]_address[15]_not_equal_5_o ;
  wire N0;
  wire nRamUB_OBUF_150;
  wire writing_inv;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<0>_177 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<0>_178 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<1>_179 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<1>_180 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<2>_181 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<2>_182 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<3>_183 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<3>_184 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<4>_185 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<4>_186 ;
  wire \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<5>_187 ;
  wire \_n0158<15>1_210 ;
  wire ledYellow111_211;
  wire ledYellow112_212;
  wire \Mmux_state[15]_PWR_1_o_Select_64_o2 ;
  wire N7;
  wire N9;
  wire N10;
  wire N11;
  wire N12;
  wire N13;
  wire N14;
  wire N15;
  wire N16;
  wire N17;
  wire N18;
  wire N19;
  wire N20;
  wire N21;
  wire N22;
  wire N23;
  wire N24;
  wire \Mcount_address_cy<1>_rt_241 ;
  wire \Mcount_address_cy<2>_rt_242 ;
  wire \Mcount_address_cy<3>_rt_243 ;
  wire \Mcount_address_cy<4>_rt_244 ;
  wire \Mcount_address_cy<5>_rt_245 ;
  wire \Mcount_address_cy<6>_rt_246 ;
  wire \Mcount_address_cy<7>_rt_247 ;
  wire \Mcount_address_cy<8>_rt_248 ;
  wire \Mcount_address_cy<9>_rt_249 ;
  wire \Mcount_address_cy<10>_rt_250 ;
  wire \Mcount_address_cy<11>_rt_251 ;
  wire \Mcount_address_cy<12>_rt_252 ;
  wire \Mcount_address_cy<13>_rt_253 ;
  wire \Mcount_address_cy<14>_rt_254 ;
  wire \Mcount_address_cy<15>_rt_255 ;
  wire \Mcount_address_cy<16>_rt_256 ;
  wire \Mcount_address_cy<17>_rt_257 ;
  wire \Mcount_address_cy<18>_rt_258 ;
  wire \Mcount_address_cy<19>_rt_259 ;
  wire \Mcount_address_xor<20>_rt_260 ;
  wire N43;
  wire N44;
  wire addrInc;
  wire \clock_BUFGP/IBUFG_2 ;
  wire VCC;
  wire GND;
  wire [15 : 0] state;
  wire [20 : 0] address;
  wire [20 : 0] Result;
  wire [0 : 0] Mcount_address_lut;
  wire [19 : 0] Mcount_address_cy;
  wire [15 : 15] _n0158_0;
  X_ONE   XST_VCC (
    .O(N0)
  );
  X_ZERO   XST_GND (
    .O(nRamUB_OBUF_150)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_15 (
    .I(state[15]),
    .CLK(_n0233),
    .O(nextState_15_92),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_12 (
    .I(state[12]),
    .CLK(_n0233),
    .O(nextState_12_95),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_14 (
    .I(state[14]),
    .CLK(_n0233),
    .O(nextState_14_93),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_13 (
    .I(state[13]),
    .CLK(_n0233),
    .O(nextState_13_94),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_9 (
    .I(state[9]),
    .CLK(_n0233),
    .O(nextState_9_98),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_11 (
    .I(state[11]),
    .CLK(_n0233),
    .O(nextState_11_96),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_10 (
    .I(state[10]),
    .CLK(_n0233),
    .O(nextState_10_97),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_6 (
    .I(state[6]),
    .CLK(_n0233),
    .O(nextState_6_101),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_8 (
    .I(state[8]),
    .CLK(_n0233),
    .O(nextState_8_99),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_7 (
    .I(state[7]),
    .CLK(_n0233),
    .O(nextState_7_100),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_5 (
    .I(state[5]),
    .CLK(_n0233),
    .O(nextState_5_102),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_4 (
    .I(state[4]),
    .CLK(_n0233),
    .O(nextState_4_103),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_1 (
    .I(\state[15]_GND_1_o_Select_62_o ),
    .CLK(_n0233),
    .O(nextState_1_108),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_0 (
    .I(\state[15]_PWR_1_o_Select_64_o ),
    .CLK(_n0233),
    .O(nextState_0_110),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_2 (
    .I(\state[15]_GND_1_o_Select_60_o ),
    .CLK(_n0233),
    .O(nextState_2_107),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_LATCHE #(
    .INIT ( 1'b0 ))
  nextState_3 (
    .I(\state[15]_GND_1_o_Select_58_o ),
    .CLK(_n0233),
    .O(nextState_3_104),
    .GE(VCC),
    .SET(GND),
    .RST(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_0 (
    .CLK(clock_BUFGP),
    .I(nextState_0_110),
    .SRST(nReset_IBUF_67),
    .O(state[0]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_1 (
    .CLK(clock_BUFGP),
    .I(nextState_1_108),
    .SRST(nReset_IBUF_67),
    .O(state[1]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_2 (
    .CLK(clock_BUFGP),
    .I(nextState_2_107),
    .SRST(nReset_IBUF_67),
    .O(state[2]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_3 (
    .CLK(clock_BUFGP),
    .I(nextState_3_104),
    .SRST(nReset_IBUF_67),
    .O(state[3]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_4 (
    .CLK(clock_BUFGP),
    .I(nextState_4_103),
    .SRST(nReset_IBUF_67),
    .O(state[4]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_5 (
    .CLK(clock_BUFGP),
    .I(nextState_5_102),
    .SRST(nReset_IBUF_67),
    .O(state[5]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_6 (
    .CLK(clock_BUFGP),
    .I(nextState_6_101),
    .SRST(nReset_IBUF_67),
    .O(state[6]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_7 (
    .CLK(clock_BUFGP),
    .I(nextState_7_100),
    .SRST(nReset_IBUF_67),
    .O(state[7]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_8 (
    .CLK(clock_BUFGP),
    .I(nextState_8_99),
    .SRST(nReset_IBUF_67),
    .O(state[8]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_9 (
    .CLK(clock_BUFGP),
    .I(nextState_9_98),
    .SRST(nReset_IBUF_67),
    .O(state[9]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_10 (
    .CLK(clock_BUFGP),
    .I(nextState_10_97),
    .SRST(nReset_IBUF_67),
    .O(state[10]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_11 (
    .CLK(clock_BUFGP),
    .I(nextState_11_96),
    .SRST(nReset_IBUF_67),
    .O(state[11]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_12 (
    .CLK(clock_BUFGP),
    .I(nextState_12_95),
    .SRST(nReset_IBUF_67),
    .O(state[12]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_13 (
    .CLK(clock_BUFGP),
    .I(nextState_13_94),
    .SRST(nReset_IBUF_67),
    .O(state[13]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_14 (
    .CLK(clock_BUFGP),
    .I(nextState_14_93),
    .SRST(nReset_IBUF_67),
    .O(state[14]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  state_15 (
    .CLK(clock_BUFGP),
    .I(nextState_15_92),
    .SRST(nReset_IBUF_67),
    .O(state[15]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_16 (
    .CLK(addrInc_BUFG_106),
    .I(Result[16]),
    .SRST(addrZero),
    .O(address[16]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_17 (
    .CLK(addrInc_BUFG_106),
    .I(Result[17]),
    .SRST(addrZero),
    .O(address[17]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_18 (
    .CLK(addrInc_BUFG_106),
    .I(Result[18]),
    .SRST(addrZero),
    .O(address[18]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_19 (
    .CLK(addrInc_BUFG_106),
    .I(Result[19]),
    .SRST(addrZero),
    .O(address[19]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_20 (
    .CLK(addrInc_BUFG_106),
    .I(Result[20]),
    .SRST(addrZero),
    .O(address[20]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .INIT ( 64'h9009000000009009 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<0>  (
    .ADR0(N24),
    .ADR1(address[0]),
    .ADR2(N23),
    .ADR3(address[1]),
    .ADR4(N22),
    .ADR5(address[2]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<0>_177 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<0>  (
    .IB(nRamUB_OBUF_150),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<0>_177 ),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<0>_178 )
  );
  X_LUT6 #(
    .INIT ( 64'h9009000000009009 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<1>  (
    .ADR0(N21),
    .ADR1(address[3]),
    .ADR2(N20),
    .ADR3(address[4]),
    .ADR4(N19),
    .ADR5(address[5]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<1>_179 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<1>  (
    .IB(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<0>_178 ),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<1>_179 ),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<1>_180 )
  );
  X_LUT6 #(
    .INIT ( 64'h9009000000009009 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<2>  (
    .ADR0(N18),
    .ADR1(address[6]),
    .ADR2(N17),
    .ADR3(address[7]),
    .ADR4(N16),
    .ADR5(address[8]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<2>_181 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<2>  (
    .IB(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<1>_180 ),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<2>_181 ),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<2>_182 )
  );
  X_LUT6 #(
    .INIT ( 64'h9009000000009009 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<3>  (
    .ADR0(N15),
    .ADR1(address[9]),
    .ADR2(N14),
    .ADR3(address[10]),
    .ADR4(N13),
    .ADR5(address[11]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<3>_183 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<3>  (
    .IB(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<2>_182 ),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<3>_183 ),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<3>_184 )
  );
  X_LUT6 #(
    .INIT ( 64'h9009000000009009 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<4>  (
    .ADR0(N12),
    .ADR1(address[12]),
    .ADR2(N11),
    .ADR3(address[13]),
    .ADR4(N10),
    .ADR5(address[14]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<4>_185 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<4>  (
    .IB(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<3>_184 ),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<4>_185 ),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<4>_186 )
  );
  X_LUT2 #(
    .INIT ( 4'h9 ))
  \Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<5>  (
    .ADR0(N9),
    .ADR1(address[15]),
    .O(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<5>_187 )
  );
  X_MUX2   \Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<5>  (
    .IB(\Mcompar_ramData[15]_address[15]_not_equal_5_o_cy<4>_186 ),
    .IA(N0),
    .SEL(\Mcompar_ramData[15]_address[15]_not_equal_5_o_lut<5>_187 ),
    .O(\ramData[15]_address[15]_not_equal_5_o )
  );
  X_MUX2   \Mcount_address_cy<0>  (
    .IB(nRamUB_OBUF_150),
    .IA(N0),
    .SEL(Mcount_address_lut[0]),
    .O(Mcount_address_cy[0])
  );
  X_XOR2   \Mcount_address_xor<0>  (
    .I0(nRamUB_OBUF_150),
    .I1(Mcount_address_lut[0]),
    .O(Result[0])
  );
  X_MUX2   \Mcount_address_cy<1>  (
    .IB(Mcount_address_cy[0]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<1>_rt_241 ),
    .O(Mcount_address_cy[1])
  );
  X_XOR2   \Mcount_address_xor<1>  (
    .I0(Mcount_address_cy[0]),
    .I1(\Mcount_address_cy<1>_rt_241 ),
    .O(Result[1])
  );
  X_MUX2   \Mcount_address_cy<2>  (
    .IB(Mcount_address_cy[1]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<2>_rt_242 ),
    .O(Mcount_address_cy[2])
  );
  X_XOR2   \Mcount_address_xor<2>  (
    .I0(Mcount_address_cy[1]),
    .I1(\Mcount_address_cy<2>_rt_242 ),
    .O(Result[2])
  );
  X_MUX2   \Mcount_address_cy<3>  (
    .IB(Mcount_address_cy[2]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<3>_rt_243 ),
    .O(Mcount_address_cy[3])
  );
  X_XOR2   \Mcount_address_xor<3>  (
    .I0(Mcount_address_cy[2]),
    .I1(\Mcount_address_cy<3>_rt_243 ),
    .O(Result[3])
  );
  X_MUX2   \Mcount_address_cy<4>  (
    .IB(Mcount_address_cy[3]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<4>_rt_244 ),
    .O(Mcount_address_cy[4])
  );
  X_XOR2   \Mcount_address_xor<4>  (
    .I0(Mcount_address_cy[3]),
    .I1(\Mcount_address_cy<4>_rt_244 ),
    .O(Result[4])
  );
  X_MUX2   \Mcount_address_cy<5>  (
    .IB(Mcount_address_cy[4]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<5>_rt_245 ),
    .O(Mcount_address_cy[5])
  );
  X_XOR2   \Mcount_address_xor<5>  (
    .I0(Mcount_address_cy[4]),
    .I1(\Mcount_address_cy<5>_rt_245 ),
    .O(Result[5])
  );
  X_MUX2   \Mcount_address_cy<6>  (
    .IB(Mcount_address_cy[5]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<6>_rt_246 ),
    .O(Mcount_address_cy[6])
  );
  X_XOR2   \Mcount_address_xor<6>  (
    .I0(Mcount_address_cy[5]),
    .I1(\Mcount_address_cy<6>_rt_246 ),
    .O(Result[6])
  );
  X_MUX2   \Mcount_address_cy<7>  (
    .IB(Mcount_address_cy[6]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<7>_rt_247 ),
    .O(Mcount_address_cy[7])
  );
  X_XOR2   \Mcount_address_xor<7>  (
    .I0(Mcount_address_cy[6]),
    .I1(\Mcount_address_cy<7>_rt_247 ),
    .O(Result[7])
  );
  X_MUX2   \Mcount_address_cy<8>  (
    .IB(Mcount_address_cy[7]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<8>_rt_248 ),
    .O(Mcount_address_cy[8])
  );
  X_XOR2   \Mcount_address_xor<8>  (
    .I0(Mcount_address_cy[7]),
    .I1(\Mcount_address_cy<8>_rt_248 ),
    .O(Result[8])
  );
  X_MUX2   \Mcount_address_cy<9>  (
    .IB(Mcount_address_cy[8]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<9>_rt_249 ),
    .O(Mcount_address_cy[9])
  );
  X_XOR2   \Mcount_address_xor<9>  (
    .I0(Mcount_address_cy[8]),
    .I1(\Mcount_address_cy<9>_rt_249 ),
    .O(Result[9])
  );
  X_MUX2   \Mcount_address_cy<10>  (
    .IB(Mcount_address_cy[9]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<10>_rt_250 ),
    .O(Mcount_address_cy[10])
  );
  X_XOR2   \Mcount_address_xor<10>  (
    .I0(Mcount_address_cy[9]),
    .I1(\Mcount_address_cy<10>_rt_250 ),
    .O(Result[10])
  );
  X_MUX2   \Mcount_address_cy<11>  (
    .IB(Mcount_address_cy[10]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<11>_rt_251 ),
    .O(Mcount_address_cy[11])
  );
  X_XOR2   \Mcount_address_xor<11>  (
    .I0(Mcount_address_cy[10]),
    .I1(\Mcount_address_cy<11>_rt_251 ),
    .O(Result[11])
  );
  X_MUX2   \Mcount_address_cy<12>  (
    .IB(Mcount_address_cy[11]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<12>_rt_252 ),
    .O(Mcount_address_cy[12])
  );
  X_XOR2   \Mcount_address_xor<12>  (
    .I0(Mcount_address_cy[11]),
    .I1(\Mcount_address_cy<12>_rt_252 ),
    .O(Result[12])
  );
  X_MUX2   \Mcount_address_cy<13>  (
    .IB(Mcount_address_cy[12]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<13>_rt_253 ),
    .O(Mcount_address_cy[13])
  );
  X_XOR2   \Mcount_address_xor<13>  (
    .I0(Mcount_address_cy[12]),
    .I1(\Mcount_address_cy<13>_rt_253 ),
    .O(Result[13])
  );
  X_MUX2   \Mcount_address_cy<14>  (
    .IB(Mcount_address_cy[13]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<14>_rt_254 ),
    .O(Mcount_address_cy[14])
  );
  X_XOR2   \Mcount_address_xor<14>  (
    .I0(Mcount_address_cy[13]),
    .I1(\Mcount_address_cy<14>_rt_254 ),
    .O(Result[14])
  );
  X_MUX2   \Mcount_address_cy<15>  (
    .IB(Mcount_address_cy[14]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<15>_rt_255 ),
    .O(Mcount_address_cy[15])
  );
  X_XOR2   \Mcount_address_xor<15>  (
    .I0(Mcount_address_cy[14]),
    .I1(\Mcount_address_cy<15>_rt_255 ),
    .O(Result[15])
  );
  X_MUX2   \Mcount_address_cy<16>  (
    .IB(Mcount_address_cy[15]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<16>_rt_256 ),
    .O(Mcount_address_cy[16])
  );
  X_XOR2   \Mcount_address_xor<16>  (
    .I0(Mcount_address_cy[15]),
    .I1(\Mcount_address_cy<16>_rt_256 ),
    .O(Result[16])
  );
  X_MUX2   \Mcount_address_cy<17>  (
    .IB(Mcount_address_cy[16]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<17>_rt_257 ),
    .O(Mcount_address_cy[17])
  );
  X_XOR2   \Mcount_address_xor<17>  (
    .I0(Mcount_address_cy[16]),
    .I1(\Mcount_address_cy<17>_rt_257 ),
    .O(Result[17])
  );
  X_MUX2   \Mcount_address_cy<18>  (
    .IB(Mcount_address_cy[17]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<18>_rt_258 ),
    .O(Mcount_address_cy[18])
  );
  X_XOR2   \Mcount_address_xor<18>  (
    .I0(Mcount_address_cy[17]),
    .I1(\Mcount_address_cy<18>_rt_258 ),
    .O(Result[18])
  );
  X_MUX2   \Mcount_address_cy<19>  (
    .IB(Mcount_address_cy[18]),
    .IA(nRamUB_OBUF_150),
    .SEL(\Mcount_address_cy<19>_rt_259 ),
    .O(Mcount_address_cy[19])
  );
  X_XOR2   \Mcount_address_xor<19>  (
    .I0(Mcount_address_cy[18]),
    .I1(\Mcount_address_cy<19>_rt_259 ),
    .O(Result[19])
  );
  X_XOR2   \Mcount_address_xor<20>  (
    .I0(Mcount_address_cy[19]),
    .I1(\Mcount_address_xor<20>_rt_260 ),
    .O(Result[20])
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \_n0158<15>1  (
    .ADR0(state[5]),
    .ADR1(state[4]),
    .ADR2(state[15]),
    .ADR3(state[14]),
    .ADR4(state[13]),
    .ADR5(state[12]),
    .O(_n0158_0[15])
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \_n0158<15>2  (
    .ADR0(state[10]),
    .ADR1(state[9]),
    .ADR2(state[8]),
    .ADR3(state[7]),
    .ADR4(state[6]),
    .ADR5(_n0158_0[15]),
    .O(\_n0158<15>1_210 )
  );
  X_LUT2 #(
    .INIT ( 4'hE ))
  \_n0158<15>3  (
    .ADR0(state[11]),
    .ADR1(\_n0158<15>1_210 ),
    .O(_n0158)
  );
  X_LUT6 #(
    .INIT ( 64'h0000000000000001 ))
  ledYellow111 (
    .ADR0(state[5]),
    .ADR1(state[4]),
    .ADR2(state[6]),
    .ADR3(state[7]),
    .ADR4(state[8]),
    .ADR5(state[9]),
    .O(ledYellow111_211)
  );
  X_LUT6 #(
    .INIT ( 64'h0000000000000001 ))
  ledYellow112 (
    .ADR0(state[11]),
    .ADR1(state[10]),
    .ADR2(state[12]),
    .ADR3(state[13]),
    .ADR4(state[14]),
    .ADR5(state[15]),
    .O(ledYellow112_212)
  );
  X_LUT6 #(
    .INIT ( 64'h011B111B015B115B ))
  \Mmux_state[15]_PWR_1_o_Select_64_o21  (
    .ADR0(state[3]),
    .ADR1(state[0]),
    .ADR2(state[1]),
    .ADR3(state[2]),
    .ADR4(\ramData[15]_address[15]_not_equal_5_o ),
    .ADR5(address[20]),
    .O(\Mmux_state[15]_PWR_1_o_Select_64_o2 )
  );
  X_LUT2 #(
    .INIT ( 4'h1 ))
  \Mmux_state[15]_GND_1_o_Select_58_o1_SW0  (
    .ADR0(state[0]),
    .ADR1(\ramData[15]_address[15]_not_equal_5_o ),
    .O(N7)
  );
  X_LUT6 #(
    .INIT ( 64'hAAAAAAA8AAEEAAA8 ))
  \Mmux_state[15]_GND_1_o_Select_58_o1  (
    .ADR0(state[3]),
    .ADR1(state[1]),
    .ADR2(address[20]),
    .ADR3(_n0158),
    .ADR4(state[2]),
    .ADR5(N7),
    .O(\state[15]_GND_1_o_Select_58_o )
  );
  X_BUF   nReset_IBUF (
    .I(nReset),
    .O(nReset_IBUF_67)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<1>_rt  (
    .ADR0(address[1]),
    .O(\Mcount_address_cy<1>_rt_241 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<2>_rt  (
    .ADR0(address[2]),
    .O(\Mcount_address_cy<2>_rt_242 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<3>_rt  (
    .ADR0(address[3]),
    .O(\Mcount_address_cy<3>_rt_243 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<4>_rt  (
    .ADR0(address[4]),
    .O(\Mcount_address_cy<4>_rt_244 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<5>_rt  (
    .ADR0(address[5]),
    .O(\Mcount_address_cy<5>_rt_245 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<6>_rt  (
    .ADR0(address[6]),
    .O(\Mcount_address_cy<6>_rt_246 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<7>_rt  (
    .ADR0(address[7]),
    .O(\Mcount_address_cy<7>_rt_247 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<8>_rt  (
    .ADR0(address[8]),
    .O(\Mcount_address_cy<8>_rt_248 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<9>_rt  (
    .ADR0(address[9]),
    .O(\Mcount_address_cy<9>_rt_249 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<10>_rt  (
    .ADR0(address[10]),
    .O(\Mcount_address_cy<10>_rt_250 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<11>_rt  (
    .ADR0(address[11]),
    .O(\Mcount_address_cy<11>_rt_251 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<12>_rt  (
    .ADR0(address[12]),
    .O(\Mcount_address_cy<12>_rt_252 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<13>_rt  (
    .ADR0(address[13]),
    .O(\Mcount_address_cy<13>_rt_253 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<14>_rt  (
    .ADR0(address[14]),
    .O(\Mcount_address_cy<14>_rt_254 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<15>_rt  (
    .ADR0(address[15]),
    .O(\Mcount_address_cy<15>_rt_255 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<16>_rt  (
    .ADR0(address[16]),
    .O(\Mcount_address_cy<16>_rt_256 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<17>_rt  (
    .ADR0(address[17]),
    .O(\Mcount_address_cy<17>_rt_257 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<18>_rt  (
    .ADR0(address[18]),
    .O(\Mcount_address_cy<18>_rt_258 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_cy<19>_rt  (
    .ADR0(address[19]),
    .O(\Mcount_address_cy<19>_rt_259 ),
    .ADR1(GND)
  );
  X_LUT2 #(
    .INIT ( 4'hA ))
  \Mcount_address_xor<20>_rt  (
    .ADR0(address[20]),
    .O(\Mcount_address_xor<20>_rt_260 ),
    .ADR1(GND)
  );
  X_LUT6 #(
    .INIT ( 64'h0400000000000000 ))
  \ledRed<15>1  (
    .ADR0(state[0]),
    .ADR1(state[1]),
    .ADR2(state[2]),
    .ADR3(state[3]),
    .ADR4(ledYellow111_211),
    .ADR5(ledYellow112_212),
    .O(ledRed_OBUF_87)
  );
  X_LUT6 #(
    .INIT ( 64'h0400000000000000 ))
  \ledGreen<15>1  (
    .ADR0(state[1]),
    .ADR1(state[0]),
    .ADR2(state[2]),
    .ADR3(state[3]),
    .ADR4(ledYellow111_211),
    .ADR5(ledYellow112_212),
    .O(ledGreen_OBUF_86)
  );
  X_LUT4 #(
    .INIT ( 16'hFDFF ))
  writing_inv1 (
    .ADR0(ledYellow111_211),
    .ADR1(state[2]),
    .ADR2(state[3]),
    .ADR3(ledYellow112_212),
    .O(writing_inv)
  );
  X_LUT6 #(
    .INIT ( 64'h0808080808080888 ))
  ledYellow1 (
    .ADR0(ledYellow111_211),
    .ADR1(ledYellow112_212),
    .ADR2(state[3]),
    .ADR3(state[0]),
    .ADR4(state[1]),
    .ADR5(state[2]),
    .O(ledYellow_OBUF_130)
  );
  X_LUT6 #(
    .INIT ( 64'h0080000000000080 ))
  addrInc1 (
    .ADR0(ledYellow111_211),
    .ADR1(ledYellow112_212),
    .ADR2(state[1]),
    .ADR3(state[3]),
    .ADR4(state[0]),
    .ADR5(state[2]),
    .O(addrInc)
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFF77FFFFFFFFF ))
  _n02331 (
    .ADR0(state[3]),
    .ADR1(ledYellow111_211),
    .ADR2(state[1]),
    .ADR3(state[0]),
    .ADR4(state[2]),
    .ADR5(ledYellow112_212),
    .O(_n0233)
  );
  X_LUT5 #(
    .INIT ( 32'h01000000 ))
  addrZero1 (
    .ADR0(state[1]),
    .ADR1(state[0]),
    .ADR2(state[3]),
    .ADR3(ledYellow111_211),
    .ADR4(ledYellow112_212),
    .O(addrZero)
  );
  X_LUT5 #(
    .INIT ( 32'hAAAAAAA6 ))
  \Mmux_state[15]_GND_1_o_Select_62_o11  (
    .ADR0(state[1]),
    .ADR1(state[0]),
    .ADR2(state[11]),
    .ADR3(\_n0158<15>1_210 ),
    .ADR4(state[3]),
    .O(\state[15]_GND_1_o_Select_62_o )
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFFFBFFFFFFFFF ))
  nRamWE1 (
    .ADR0(state[1]),
    .ADR1(state[0]),
    .ADR2(ledYellow111_211),
    .ADR3(state[3]),
    .ADR4(state[2]),
    .ADR5(ledYellow112_212),
    .O(nRamWE_OBUF_109)
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFF77FFFFFFFFF ))
  nRamOE1 (
    .ADR0(ledYellow111_211),
    .ADR1(ledYellow112_212),
    .ADR2(state[0]),
    .ADR3(state[1]),
    .ADR4(state[3]),
    .ADR5(state[2]),
    .O(nRamOE_OBUF_129)
  );
  X_LUT6 #(
    .INIT ( 64'hFFFFFFFFA7FFFFFF ))
  nRamCE1 (
    .ADR0(state[1]),
    .ADR1(state[2]),
    .ADR2(state[0]),
    .ADR3(ledYellow111_211),
    .ADR4(ledYellow112_212),
    .ADR5(state[3]),
    .O(nRamCE_OBUF_131)
  );
  X_LUT5 #(
    .INIT ( 32'hABABAAA8 ))
  \Mmux_state[15]_PWR_1_o_Select_64_o22  (
    .ADR0(state[0]),
    .ADR1(state[11]),
    .ADR2(\_n0158<15>1_210 ),
    .ADR3(state[3]),
    .ADR4(\Mmux_state[15]_PWR_1_o_Select_64_o2 ),
    .O(\state[15]_PWR_1_o_Select_64_o )
  );
  X_MUX2   \Mmux_state[15]_GND_1_o_Select_60_o2  (
    .IA(N43),
    .IB(N44),
    .SEL(state[2]),
    .O(\state[15]_GND_1_o_Select_60_o )
  );
  X_LUT6 #(
    .INIT ( 64'h0010010000000100 ))
  \Mmux_state[15]_GND_1_o_Select_60_o2_F  (
    .ADR0(state[11]),
    .ADR1(\_n0158<15>1_210 ),
    .ADR2(state[1]),
    .ADR3(state[3]),
    .ADR4(address[20]),
    .ADR5(state[0]),
    .O(N43)
  );
  X_LUT6 #(
    .INIT ( 64'hFEFFFEFFFEFFFFFF ))
  \Mmux_state[15]_GND_1_o_Select_60_o2_G  (
    .ADR0(state[3]),
    .ADR1(state[11]),
    .ADR2(\_n0158<15>1_210 ),
    .ADR3(state[1]),
    .ADR4(\ramData[15]_address[15]_not_equal_5_o ),
    .ADR5(state[0]),
    .O(N44)
  );
  X_CKBUF   addrInc_BUFG (
    .O(addrInc_BUFG_106),
    .I(addrInc)
  );
  X_INV   \Mcount_address_lut<0>_INV_0  (
    .I(address[0]),
    .O(Mcount_address_lut[0])
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_15 (
    .CLK(addrInc_BUFG_106),
    .I(Result[15]),
    .SRST(addrZero),
    .O(address[15]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_14 (
    .CLK(addrInc_BUFG_106),
    .I(Result[14]),
    .SRST(addrZero),
    .O(address[14]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_13 (
    .CLK(addrInc_BUFG_106),
    .I(Result[13]),
    .SRST(addrZero),
    .O(address[13]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_12 (
    .CLK(addrInc_BUFG_106),
    .I(Result[12]),
    .SRST(addrZero),
    .O(address[12]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_11 (
    .CLK(addrInc_BUFG_106),
    .I(Result[11]),
    .SRST(addrZero),
    .O(address[11]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_10 (
    .CLK(addrInc_BUFG_106),
    .I(Result[10]),
    .SRST(addrZero),
    .O(address[10]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_9 (
    .CLK(addrInc_BUFG_106),
    .I(Result[9]),
    .SRST(addrZero),
    .O(address[9]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_8 (
    .CLK(addrInc_BUFG_106),
    .I(Result[8]),
    .SRST(addrZero),
    .O(address[8]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_7 (
    .CLK(addrInc_BUFG_106),
    .I(Result[7]),
    .SRST(addrZero),
    .O(address[7]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_6 (
    .CLK(addrInc_BUFG_106),
    .I(Result[6]),
    .SRST(addrZero),
    .O(address[6]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_5 (
    .CLK(addrInc_BUFG_106),
    .I(Result[5]),
    .SRST(addrZero),
    .O(address[5]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_4 (
    .CLK(addrInc_BUFG_106),
    .I(Result[4]),
    .SRST(addrZero),
    .O(address[4]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_3 (
    .CLK(addrInc_BUFG_106),
    .I(Result[3]),
    .SRST(addrZero),
    .O(address[3]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_2 (
    .CLK(addrInc_BUFG_106),
    .I(Result[2]),
    .SRST(addrZero),
    .O(address[2]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_1 (
    .CLK(addrInc_BUFG_106),
    .I(Result[1]),
    .SRST(addrZero),
    .O(address[1]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .INIT ( 1'b0 ))
  address_0 (
    .CLK(addrInc_BUFG_106),
    .I(Result[0]),
    .SRST(addrZero),
    .O(address[0]),
    .CE(VCC),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_BPAD   \ramData<15>  (
    .PAD(ramData[15])
  );
  X_BPAD   \ramData<14>  (
    .PAD(ramData[14])
  );
  X_BPAD   \ramData<13>  (
    .PAD(ramData[13])
  );
  X_BPAD   \ramData<12>  (
    .PAD(ramData[12])
  );
  X_BPAD   \ramData<11>  (
    .PAD(ramData[11])
  );
  X_BPAD   \ramData<10>  (
    .PAD(ramData[10])
  );
  X_BPAD   \ramData<9>  (
    .PAD(ramData[9])
  );
  X_BPAD   \ramData<8>  (
    .PAD(ramData[8])
  );
  X_BPAD   \ramData<7>  (
    .PAD(ramData[7])
  );
  X_BPAD   \ramData<6>  (
    .PAD(ramData[6])
  );
  X_BPAD   \ramData<5>  (
    .PAD(ramData[5])
  );
  X_BPAD   \ramData<4>  (
    .PAD(ramData[4])
  );
  X_BPAD   \ramData<3>  (
    .PAD(ramData[3])
  );
  X_BPAD   \ramData<2>  (
    .PAD(ramData[2])
  );
  X_BPAD   \ramData<1>  (
    .PAD(ramData[1])
  );
  X_BPAD   \ramData<0>  (
    .PAD(ramData[0])
  );
  X_IPAD   clock_230 (
    .PAD(clock)
  );
  X_IPAD   nReset_231 (
    .PAD(nReset)
  );
  X_OPAD   nRamOE_232 (
    .PAD(nRamOE)
  );
  X_OPAD   nRamWE_233 (
    .PAD(nRamWE)
  );
  X_OPAD   nRamCE_234 (
    .PAD(nRamCE)
  );
  X_OPAD   nRamLB_235 (
    .PAD(nRamLB)
  );
  X_OPAD   nRamUB_236 (
    .PAD(nRamUB)
  );
  X_OPAD   ledRed_237 (
    .PAD(ledRed)
  );
  X_OPAD   ledYellow_238 (
    .PAD(ledYellow)
  );
  X_OPAD   ledGreen_239 (
    .PAD(ledGreen)
  );
  X_CKBUF   \clock_BUFGP/BUFG  (
    .I(\clock_BUFGP/IBUFG_2 ),
    .O(clock_BUFGP)
  );
  X_CKBUF   \clock_BUFGP/IBUFG  (
    .I(clock),
    .O(\clock_BUFGP/IBUFG_2 )
  );
  X_BUF   \ramData_15_IOBUF/IBUF  (
    .I(ramData[15]),
    .O(N9)
  );
  X_BUF   \ramData_14_IOBUF/IBUF  (
    .I(ramData[14]),
    .O(N10)
  );
  X_BUF   \ramData_13_IOBUF/IBUF  (
    .I(ramData[13]),
    .O(N11)
  );
  X_BUF   \ramData_12_IOBUF/IBUF  (
    .I(ramData[12]),
    .O(N12)
  );
  X_BUF   \ramData_11_IOBUF/IBUF  (
    .I(ramData[11]),
    .O(N13)
  );
  X_BUF   \ramData_10_IOBUF/IBUF  (
    .I(ramData[10]),
    .O(N14)
  );
  X_BUF   \ramData_9_IOBUF/IBUF  (
    .I(ramData[9]),
    .O(N15)
  );
  X_BUF   \ramData_8_IOBUF/IBUF  (
    .I(ramData[8]),
    .O(N16)
  );
  X_BUF   \ramData_7_IOBUF/IBUF  (
    .I(ramData[7]),
    .O(N17)
  );
  X_BUF   \ramData_6_IOBUF/IBUF  (
    .I(ramData[6]),
    .O(N18)
  );
  X_BUF   \ramData_5_IOBUF/IBUF  (
    .I(ramData[5]),
    .O(N19)
  );
  X_BUF   \ramData_4_IOBUF/IBUF  (
    .I(ramData[4]),
    .O(N20)
  );
  X_BUF   \ramData_3_IOBUF/IBUF  (
    .I(ramData[3]),
    .O(N21)
  );
  X_BUF   \ramData_2_IOBUF/IBUF  (
    .I(ramData[2]),
    .O(N22)
  );
  X_BUF   \ramData_1_IOBUF/IBUF  (
    .I(ramData[1]),
    .O(N23)
  );
  X_BUF   \ramData_0_IOBUF/IBUF  (
    .I(ramData[0]),
    .O(N24)
  );
  X_OBUFT   \ramData_15_IOBUF/OBUFT  (
    .I(address[15]),
    .CTL(writing_inv),
    .O(ramData[15])
  );
  X_OBUFT   \ramData_14_IOBUF/OBUFT  (
    .I(address[14]),
    .CTL(writing_inv),
    .O(ramData[14])
  );
  X_OBUFT   \ramData_13_IOBUF/OBUFT  (
    .I(address[13]),
    .CTL(writing_inv),
    .O(ramData[13])
  );
  X_OBUFT   \ramData_12_IOBUF/OBUFT  (
    .I(address[12]),
    .CTL(writing_inv),
    .O(ramData[12])
  );
  X_OBUFT   \ramData_11_IOBUF/OBUFT  (
    .I(address[11]),
    .CTL(writing_inv),
    .O(ramData[11])
  );
  X_OBUFT   \ramData_10_IOBUF/OBUFT  (
    .I(address[10]),
    .CTL(writing_inv),
    .O(ramData[10])
  );
  X_OBUFT   \ramData_9_IOBUF/OBUFT  (
    .I(address[9]),
    .CTL(writing_inv),
    .O(ramData[9])
  );
  X_OBUFT   \ramData_8_IOBUF/OBUFT  (
    .I(address[8]),
    .CTL(writing_inv),
    .O(ramData[8])
  );
  X_OBUFT   \ramData_7_IOBUF/OBUFT  (
    .I(address[7]),
    .CTL(writing_inv),
    .O(ramData[7])
  );
  X_OBUFT   \ramData_6_IOBUF/OBUFT  (
    .I(address[6]),
    .CTL(writing_inv),
    .O(ramData[6])
  );
  X_OBUFT   \ramData_5_IOBUF/OBUFT  (
    .I(address[5]),
    .CTL(writing_inv),
    .O(ramData[5])
  );
  X_OBUFT   \ramData_4_IOBUF/OBUFT  (
    .I(address[4]),
    .CTL(writing_inv),
    .O(ramData[4])
  );
  X_OBUFT   \ramData_3_IOBUF/OBUFT  (
    .I(address[3]),
    .CTL(writing_inv),
    .O(ramData[3])
  );
  X_OBUFT   \ramData_2_IOBUF/OBUFT  (
    .I(address[2]),
    .CTL(writing_inv),
    .O(ramData[2])
  );
  X_OBUFT   \ramData_1_IOBUF/OBUFT  (
    .I(address[1]),
    .CTL(writing_inv),
    .O(ramData[1])
  );
  X_OBUFT   \ramData_0_IOBUF/OBUFT  (
    .I(address[0]),
    .CTL(writing_inv),
    .O(ramData[0])
  );
  X_OBUF   nRamOE_OBUF (
    .I(nRamOE_OBUF_129),
    .O(nRamOE)
  );
  X_OBUF   nRamWE_OBUF (
    .I(nRamWE_OBUF_109),
    .O(nRamWE)
  );
  X_OBUF   nRamCE_OBUF (
    .I(nRamCE_OBUF_131),
    .O(nRamCE)
  );
  X_OBUF   nRamLB_OBUF (
    .I(nRamUB_OBUF_150),
    .O(nRamLB)
  );
  X_OBUF   nRamUB_OBUF (
    .I(nRamUB_OBUF_150),
    .O(nRamUB)
  );
  X_OBUF   ledRed_OBUF (
    .I(ledRed_OBUF_87),
    .O(ledRed)
  );
  X_OBUF   ledYellow_OBUF (
    .I(ledYellow_OBUF_130),
    .O(ledYellow)
  );
  X_OBUF   ledGreen_OBUF (
    .I(ledGreen_OBUF_86),
    .O(ledGreen)
  );
  X_ONE   NlwBlock_memtest_VCC (
    .O(VCC)
  );
  X_ZERO   NlwBlock_memtest_GND (
    .O(GND)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

